//
//  MCNetWorkProxy.h
//  MCTank
//
//  Created by mac on 2018/7/12.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>

typedef NS_ENUM(NSInteger, MCPacketCodes) {
    NETWORK_ACK,                    // no packet
    NETWORK_COINTOSS,               // decide who is going to be the server
    NETWORK_MOVE_EVENT,             // send position
    NETWORK_FIRE_EVENT,             // send fire
    NETWORK_HEARTBEAT               // send of entire state at regular intervals
};

@interface MCNetWorkProxy : NSObject <MCSessionDelegate>

+ (instancetype)proxy;

- (void)receiveData:(NSData *)data fromPeer:(NSString *)peer inSession:(MCSession *)session context:(void *)context;

- (void)sendNetworkPacket:(MCSession *)session packetID:(int)packetID withData:(void *)data ofLength:(int)length reliable:(BOOL)howtosend;

@end
