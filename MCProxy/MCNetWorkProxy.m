//
//  MCNetWorkProxy.m
//  MCTank
//
//  Created by mac on 2018/7/12.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "MCNetWorkProxy.h"

#define kMaxTankPacketSize 1024

@implementation MCNetWorkProxy {
    int gamePacketNumber;
    MCPeerID *gamePeerId;
}

static MCNetWorkProxy *proxy;

+ (instancetype)proxy {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        proxy = [[MCNetWorkProxy alloc]init];
        proxy->gamePacketNumber = 0;
    });
    return proxy;
}

- (void)receiveData:(NSData *)data fromPeer:(NSString *)peer inSession:(MCSession *)session context:(void *)context {
    
}

- (void)sendNetworkPacket:(MCSession *)session packetID:(int)packetID withData:(void *)data ofLength:(int)length reliable:(BOOL)howtosend {
    // the packet we'll send is resued
    static unsigned char networkPacket[kMaxTankPacketSize];
    const unsigned int packetHeaderSize = 2 * sizeof(int); // we have two "ints" for our header
    
    if(length < (kMaxTankPacketSize - packetHeaderSize)) { // our networkPacket buffer size minus the size of the header info
        int *pIntData = (int *)&networkPacket[0];
        // header info
        pIntData[0] = gamePacketNumber++;
        pIntData[1] = packetID;
        // copy data in after the header
        memcpy( &networkPacket[packetHeaderSize], data, length );
        
        NSData *packet = [NSData dataWithBytes: networkPacket length: (length+8)];
        if(howtosend == YES) {
            [session sendData:packet toPeers:[NSArray arrayWithObject:gamePeerId] withMode:MCSessionSendDataReliable error:nil];
        } else {
            [session sendData:packet toPeers:[NSArray arrayWithObject:gamePeerId] withMode:MCSessionSendDataUnreliable error:nil];
        }
    }
}

#pragma mark MCSessionDelegate Methods

// we've gotten a state change in the session
- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state {
    
}

@end
