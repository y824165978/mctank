//
//  MCConstants.m
//  MCTank
//
//  Created by mac on 2018/7/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import "MCConstants.h"

// the cool "completely change the game" variables
const float kTankSpeed = 1.0f;
const float kMissileSpeed = 3.0f;
const float kTankTurnSpeed = 0.1f;
const float kHeartbeatTimeMaxDelay = 2.0f;

@implementation MCConstants

@end
