//
//  MCConstants.h
//  MCTank
//
//  Created by mac on 2018/7/13.
//  Copyright © 2018年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

//
// various states the game can get into
//
typedef NS_ENUM(NSInteger, MCGameStates) {
    kStateStartGame,
    kStatePicker,
    kStateMultiplayer,
    kStateMultiplayerCointoss,
    kStateMultiplayerReconnect
};

//
// for the sake of simplicity tank1 is the server and tank2 is the client
//
typedef NS_ENUM(NSInteger, MCGameNetwork) {
    kServer,
    kClient
};

// the cool "completely change the game" variables
FOUNDATION_EXPORT const float kTankSpeed;
FOUNDATION_EXPORT const float kMissileSpeed;
FOUNDATION_EXPORT const float kTankTurnSpeed;
FOUNDATION_EXPORT const float kHeartbeatTimeMaxDelay;
#define missileLife 60

// strings for game label
#define kStartLabel    @"Tap to Start"
#define kBlueLabel    @"You're Blue"
#define kRedLabel    @"You're Red"

// GameKit Session ID for app
#define kTankSessionID @"mctank"

@interface MCConstants : NSObject

@end
